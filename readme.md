# A Simple REST API in Node.js + express

This is a REST API for a sample Task app illustrating the relatioship with OpenAPI (Swagger) documentation and the code implementing it. The petshop sample is more comprehensive, but may be a lot to take in at first, hence this smaller and simpler example.

## Installation and running instructions

To install the dependencies (express), run the install command:

```
npm install
```

To run the application on localhost, run:

```
node index.js
```

To verify the app is running, visit:

```
http://localhost:3000/todos
```

## Features

The API documentation defines the following endpoints:

```
GET /todos  - return all todos
    /todos?completed=[boolean]  - filter based on completion status
    /todos?limit=[int]  - limit number of results

POST /todos  - add a new task
     expects a json body with description[string] and completed=[boolean]
     example: {"description": "A new task", "completed": false}

PATCH /todos/{id}  - updates the completion status of a task based on the body data
      example: {"completed": true}

DELETE /todos/{id}  - deletes a task
```

Check out the full OpenAPI documentation for more details.

## Testing the API endpoints

Once you have the Node.js server running, you can use cUrl or [Postman](https://www.postman.com) to test the endpoints.

## Notes

To avoid complicating the sample code, there are some features presented in the documentation that are missing from the implementation:

- Limiting the number of items based on the optional ```limit``` query parameter. You may see, if you can add this on your own.
- The ```PATCH``` method now expects only the completion status to be changed. However, ```PATCH``` could be used to change many properties. You may see, if you can make this change. HINT: ```let updated = {...old, ...new};``` Ultimately, however, this depends on the type of application you want to create. For complete replacement, change to ```PUT```.

## OpenAPI documentation

You can see a more interactive approach to API documentation by pasting this into [http://editor.swagger.io](http://editor.swagger.io).

```
openapi: 3.0.0
info:
  description: A small API example of a Todo app using OpenAPI 3.0
  title: A Todo list application
  version: 1.0.0
paths:
  /todos:
    get:
      tags:
        - todos
      parameters:
        - name: completed
          in: query
          schema:
            type: boolean
            default: false
          required: false
        - name: limit
          in: query
          schema:
            type: integer
            format: int32
            default: 20
      responses:
        200:
          description: list the todo operations
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Item'
    post:
      tags:
        - todos
      operationId: addOne
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Item'
      responses:
        201:
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Item'
  /todos/{id}:
    parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
    patch:
      tags:
        - todos
      operationId: updateOne
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Item'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Item'
        404:
          description: Invalid id
    delete:
      tags:
        - todos
      operationId: destroyOne
      responses:
        204:
          description: Deleted
        404:
          description: Invalid id
components:
  schemas:
    Item:
      type: object
      required:
        - description
      properties:
        id:
          type: integer
          format: int64
          readOnly: true
        description:
          type: string
          minLength: 1
        completed:
          type: boolean
```
