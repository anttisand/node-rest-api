const express = require('express'); //This simplifies handling of requests and responses

const app = express(); //Create a new express server

const bodyParser = require('body-parser'); //Makes parsin the request easier

app.use(bodyParser.json()); //Set application/json as the default

let todos = require('./tasks.json');

app.get('/todos', (request, response) => {
    let completed = request.query.completed; //Filter todos based on completion status
    let limit = request.query.limit; //TODO: Limit the number of items to return

    if(completed !== undefined) { //If we are filtering based on completion status, add this filtering logic
        completed = (completed === 'true'); //Convert string to boolean
        let filteredTodos = todos.filter(todo => todo.completed == completed);
        response.status(200).json(filteredTodos);
    } else {
        response.status(200).json(todos);
    }
});

app.post('/todos', (request, response) => {
    let newTask = request.body; //Assume we got a json object in body
    newTask.id = todos.length + 1; //Set an id for the new task
    todos.push(newTask);
    response.status(201).json(newTask);
});

app.patch('/todos/:id', (request, response) => {
    let id = parseInt(request.params.id);
    if(todos[id - 1]) {
        let completionStatus = request.body;
        todos[id - 1].completed = completionStatus.completed;
        response.status(204).send(); //Send just a status code, 204 for no content
    } else {
        response.status(404).send();
    }
});

app.delete('/todos/:id', (request, response) => {
    let id = parseInt(request.params.id);
    if(todos.filter(todo => todo.id === id).length !== 0) {
        todos = todos.filter(todo => todo.id !== id);
        response.status(204).send();
    } else {
        response.status(404).send();
    }
});

const server = app.listen(3000, () => {
    console.log(`Server is running on port ${server.address().port}`);
});